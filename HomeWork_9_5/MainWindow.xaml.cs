﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HomeWork_9_5
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButtonSplit_Click(object sender, RoutedEventArgs e)
        {
            if (TextBoxSplit.Text.Length > 0) 
            {                                
                ListBoxOutput.ItemsSource = TextBoxSplit.Text.Split(' ');
                ListBoxOutput.Items.Refresh();
            } else
            {
                MessageBox.Show("Input text for splitting");
            }
        }

        private void ButtonReorder_Click(object sender, RoutedEventArgs e)
        {
            if (TextBoxReorder.Text.Length > 0)
            {
                string[] words = TextBoxReorder.Text.Split(' ');
                Array.Reverse(words);
                LabelOutput.Content = string.Join(' ', words);                
            }
            else
            {
                MessageBox.Show("Input text for reordering");
            }
        }
    }
}
